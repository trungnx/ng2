import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'power-field',
    templateUrl: 'app/components/hero-form/power-field/power-field.component.html'
})
export class PowerFieldComponent {
    @Input('group')
    public addHeroForm: FormGroup;
}