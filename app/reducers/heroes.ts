import {ActionReducer, Action} from "@ngrx/store";
import {Hero} from "../entities/hero";
import {HERO_ADD_SUCCESSFUL, HERO_LOAD_SUCCESSFUL, HERO_DELETE_SUCCESSFUL} from "../actions/heros.action";

export const heroes: ActionReducer<Hero[]> = (state: Hero[] = [], action: Action) => {
    switch (action.type) {
        case HERO_ADD_SUCCESSFUL:
            return [...state, action.payload];

        case HERO_LOAD_SUCCESSFUL:
            return state.concat(action.payload);

        case HERO_DELETE_SUCCESSFUL:
            return state.filter(hero => hero.id !== action.payload.id);

        default:
            return state;
    }
};